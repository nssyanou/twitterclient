package com.example.kensyu2.twitterclient;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.view.View.OnClickListener;

public class MainActivity extends Activity implements OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (!TwitterUtils.hasAccessToken(this)) {
            Intent intent = new Intent(this, TwitterOAuthActivity.class);
            startActivity(intent);
            finish();
        }

        Button loadTimeLineBtn = (Button) findViewById(R.id.reloadBtn);
        loadTimeLineBtn.setOnClickListener(this);

        Button tweetActivityBtn = (Button) findViewById(R.id.tweetBtn);
        tweetActivityBtn.setOnClickListener(this);

        Button searchBtn = (Button) findViewById(R.id.searchBtn);
        searchBtn.setOnClickListener(this);
    }
        public void onClick(View v){
            switch(v.getId()){
                case R.id.reloadBtn:
                    startActivity(new Intent(this, TweetTimeLineActivity.class));
                    break;
                case R.id.tweetBtn:
                    startActivity(new Intent(this, TweetActivity.class));
                    break;
                case R.id.searchBtn:
                    break;
            }
        }
}
