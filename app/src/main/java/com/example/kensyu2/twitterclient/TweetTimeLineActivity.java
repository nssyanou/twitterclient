package com.example.kensyu2.twitterclient;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;

public class TweetTimeLineActivity extends AppCompatActivity {

    private Twitter twitter;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tweet_timeline);
        reloadTimeLine();
    }

    public void reloadTimeLine(){
        twitter = TwitterUtils.getTwitterInstance(this);
        try {
            //TLの取得
            ResponseList<Status> homeTl = twitter.getHomeTimeline();

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1);

            for (Status status : homeTl) {
                //つぶやきのユーザーIDの取得
                String userName = status.getUser().getScreenName();
                //つぶやきの取得
                String tweet = status.getText();
                adapter.add("ユーザーID：" + userName + "\r\n" + "tweet：" + tweet);
            }
            ListView TimeLineList = (ListView) findViewById(R.id.TimeLineList);
            TimeLineList.setAdapter(adapter);
        } catch (TwitterException e) {
            e.printStackTrace();
            if (e.isCausedByNetworkIssue()) {
                Toast.makeText(getApplicationContext(), "ネットワークに接続して下さい", Toast.LENGTH_LONG);
            } else {
                Toast.makeText(getApplicationContext(), "エラーが発生しました。", Toast.LENGTH_LONG);
            }
        }
    }
}