package com.example.kensyu2.twitterclient;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;


public class TweetActivity extends Activity {

    private Twitter Twitter;
    EditText tweetText;
    Button updateStatusBtn;

        protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tweet);

        tweetText = (EditText)findViewById(R.id.tweetText);
        updateStatusBtn = (Button)findViewById(R.id.updateStatusBtn);
        updateStatusBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String text = tweetText.getText().toString();
                tweet();
            }
        });
    }
    private void tweet() {
        Twitter = TwitterUtils.getTwitterInstance(this);
        AsyncTask<String, Void, Boolean> task = new AsyncTask<String, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(String... params) {
                try {
                    Twitter.updateStatus(params[0]);
                    return true;
                } catch (TwitterException e) {
                    e.printStackTrace();
                    return false;
                }
            }

            @Override
            protected void onPostExecute(Boolean result) {
                if (result) {
                    showToast("ツイートが完了しました！");
                    finish();
                } else {
                    showToast("ツイートに失敗しました。。。");
                }
            }
        };
        task.execute(tweetText.getText().toString());
    }

    private void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }
}
